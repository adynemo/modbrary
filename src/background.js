'use strict'

import 'regenerator-runtime';
import { app, protocol, BrowserWindow, ipcMain, shell, Menu } from 'electron'
import { createProtocol } from 'vue-cli-plugin-electron-builder/lib'
import installExtension, { VUEJS3_DEVTOOLS } from 'electron-devtools-installer'
import path from 'path';
import * as core from './utils/core';
import * as settings from './utils/settings';
import * as mods from './utils/mods';
import * as categories from './utils/categories';
import * as tags from './utils/tags';
import * as transfer from './utils/transfer';
import { fetchConvertedImages } from './utils/images';
import log from 'electron-log';
import fetch from 'node-fetch';

log.transports.file.resolvePath = () => core.logFullPath;
const isDevelopment = process.env.NODE_ENV !== 'production'
const coreLog = log.scope('core');
const settingsLog = log.scope('settings');
const modLog = log.scope('mod');
const catLog = log.scope('cat');
const tagLog = log.scope('tag');
let debug = isDevelopment;

const isDebug = async () => {
  const result = await settings.fetch();
  if (!result.success) {
    settingsLog.error(result);
  }
  debug = result.data?.debug;
}

// Scheme must be registered before the app is ready
protocol.registerSchemesAsPrivileged([
  { scheme: 'app', privileges: { secure: true, standard: true } }
])

let win = null;
async function createWindow() {
  try {
    await isDebug();
  } catch (e) {
    settingsLog.warn('Unable to fetch settings to define debug mode');
  }
  // Create the browser window.
  if (!debug) {
    Menu.setApplicationMenu(null);
  }
  win = new BrowserWindow({
    width: 1280,
    height: 720,
    webPreferences: {
      // Use pluginOptions.nodeIntegration, leave this alone
      // See nklayman.github.io/vue-cli-plugin-electron-builder/guide/security.html#node-integration for more info
      nodeIntegration: process.env.ELECTRON_NODE_INTEGRATION,
      contextIsolation: !process.env.ELECTRON_NODE_INTEGRATION,
      enableRemoteModule: true,
      preload: path.join(__dirname, 'preload.js'),
    }
  })

  if (process.env.WEBPACK_DEV_SERVER_URL) {
    // Load the url of the dev server if in development mode
    await win.loadURL(process.env.WEBPACK_DEV_SERVER_URL)
    if (!process.env.IS_TEST) win.webContents.openDevTools()
  } else {
    createProtocol('app')
    // Load the index.html when not in development
    win.loadURL('app://./index.html')
  }
  coreLog.debug('Hello Modby!')
}

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    coreLog.debug('Bye Modby!')
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) createWindow()
})

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', async () => {
  if (isDevelopment && !process.env.IS_TEST) {
    // Install Vue Devtools
    try {
      await installExtension(VUEJS3_DEVTOOLS)
    } catch (e) {
      log.error('Vue Devtools failed to install:', e.toString())
    }
  }
  createWindow()
})

// Exit cleanly on request from parent process in development mode.
if (isDevelopment) {
  if (process.platform === 'win32') {
    process.on('message', (data) => {
      if (data === 'graceful-exit') {
        app.quit()
      }
    })
  } else {
    process.on('SIGTERM', () => {
      app.quit()
    })
  }
}

/** setup */
ipcMain.on('app:setup', async () => {
  const result = await core.setup();
  if (!result.success) {
    coreLog.error(result);
  }
  win.webContents.send('app:setup', result)
});

ipcMain.on('app:version', async () => {
  win.webContents.send('app:version', app.getVersion());
});

ipcMain.on('app:check-update', () => {
  fetch(`https://gitlab.com/adynemo/modbrary/-/blob/main/build/Modbrary%20Setup%20${app.getVersion()}.exe`, {
    redirect: 'manual'
  }).then(resp => {
    let upToDate = false;
    if (200 === resp.status) {
      upToDate = true;
    }
    win.webContents.send('app:check-update', upToDate);
  }).catch(error => {
    coreLog.warn('Unable to check update', error);
    win.webContents.send('app:check-update', undefined);
  })
});

ipcMain.on('app:download-update', () => {
  shell.openExternal('https://gitlab.com/adynemo/modbrary/-/blob/main/build');
});

/** settings */
ipcMain.on('settings:save', async (event, arg) => {
  const result = await settings.save(arg);
  if (!result.success) {
    settingsLog.error(result);
  }
  win.webContents.send('settings:save', result)
});

ipcMain.on('settings:fetch', async () => {
  const result = await settings.fetch();
  if (!result.success) {
    settingsLog.error(result);
  }
  win.webContents.send('settings:fetch', result)
});

/** mods */
ipcMain.on('mods:tree', async () => {
  let result = {
    success: false,
    message: 'Unable to build mods tree',
    reason: null
  };

  const appSettings = await settings.fetch();

  if (!appSettings.success) {
    result.reason = appSettings.reason;
  }

  if (!appSettings.data.modsPath) {
    result.reason = 'modsPath setting is not set';
  }

  const tree = mods.getTree(appSettings.data.modsPath);
  if (!tree) {
    result.reason = `Mods path not exists (${appSettings.data.modsPath})`;
  } else {
    result = {
      success: true,
      message: 'Mods tree built',
      data: tree
    };
  }

  if (!result.success) {
    modLog.error(result);
  }

  win.webContents.send('mods:tree', result)
});

ipcMain.on('mods:sync',  (event, args) => {
  mods.sync(args?.path)
    .then(result => {
      win.webContents.send('mods:sync', result)
    })
    .catch(e => {
      modLog.error(e);
    });
});

ipcMain.on('mod:edit', async (event, arg) => {
  const result = await mods.edit(arg.mod, arg.image, arg.isDefault);
  if (!result.success) {
    modLog.error(result);
  }
  win.webContents.send('mod:edit', result)
});

ipcMain.on('mod:delete', async (event, arg) => {
  const result = await mods.remove(arg);
  if (!result.success) {
    modLog.error(result);
  }
  win.webContents.send('mod:delete', result)
});

ipcMain.on('mod:show-file', (event, arg) => {
  if (arg.path) {
    shell.showItemInFolder(arg.path);
  }
})

ipcMain.on('mods:fetch-all',  (event, args) => {
  const result = mods.getAll(args?.type !== 'transfer');
  if (!result.success) {
    modLog.error(result);
  }
  win.webContents.send('mods:fetch-all', result)
});

ipcMain.on('mods:fetch-image',  async (event, args) => {
  const result = await fetchConvertedImages(args.mods, args.type !== 'transfer');
  if (!result.success) {
    modLog.error(result);
  }
  win.webContents.send('mods:fetch-image', result)
});

ipcMain.on('mods:transfer',  (event, args) => {
  transfer.sync(args.mods)
    .then(result => {
      win.webContents.send('mods:transfer', result)
    })
    .catch(e => {
      modLog.error(e);
    });
});

ipcMain.on('mods:transfer-reset',  () => {
  transfer.reset();
});

/** categories */
ipcMain.on('category:save', async (event, arg) => {
  categories.save(arg)
    .then(result => win.webContents.send('category:save', result))
    .catch(error => catLog.error(error));
});

ipcMain.on('categories:fetch', async () => {
  categories.fetch()
    .then(result => win.webContents.send('categories:fetch', result))
    .catch(error => catLog.error(error));
});

ipcMain.on('categories:auto-add', async () => {
  categories.sync()
    .then(result => win.webContents.send('categories:auto-add', result))
    .catch(error => catLog.error(error));
});

/** tags */
ipcMain.on('tag:save', (event, arg) => {
  tags.save(arg)
    .then(result => win.webContents.send('tag:save', result))
    .catch(e => tagLog.error(e));
});

ipcMain.on('tags:fetch', () => {
  tags.fetch()
    .then(result => win.webContents.send('tags:fetch', result))
    .catch(e => tagLog.error(e));
});

/** others */
ipcMain.on('web-search:mod', (event, arg) => {
  settings.getSearchEngineURI()
    .then(result => {
      const url = `${result.data.uri}Sims 4 mods ${arg.name}`;
      shell.openExternal(url)
    });
});
