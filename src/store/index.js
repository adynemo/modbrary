import { createStore } from 'vuex'

const defaultState = {
  active: false,
  color: '',
  message: '',
  link: '',
  closable: false,
};

const snackbar = {
  namespaced: true,
  state: () => ({...defaultState}),
  mutations: {
    activate(state) {
      state.active = true;
    },
    setColor(state, color) {
      state.color = color;
    },
    setMessage(state, message) {
      state.message = message;
    },
    setLink(state, link) {
      state.link = link;
    },
    setClosable(state, closable) {
      state.closable = closable;
    },
    reset(state) {
      state.active = defaultState.active;
      state.color = defaultState.color;
      state.message = defaultState.message;
      state.link = defaultState.link;
      state.closable = defaultState.closable;
    }
  },
  actions: {
    display({ commit }, {type, message, link, closable}) {
      closable = !!closable;
      commit('setColor', type);
      commit('setMessage', message);
      if (link) {
        commit('setLink', link);
      }
      commit('setClosable', closable);
      commit('activate');
    },
    reset({ commit }) {
      commit('reset');
    }
  },
}

export default createStore({
  modules: {
    snackbar,
  }
})
