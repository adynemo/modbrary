import { contextBridge, ipcRenderer } from 'electron'

const validChannels = [
	'app:setup',
	'app:version',
	'app:check-update',
	'app:download-update',
	'settings:save',
	'settings:fetch',
	'mods:tree',
	'mods:sync',
	'mods:transfer',
	'mods:transfer-reset',
	'mods:fetch-all',
	'mods:fetch-image',
	'mod:edit',
	'mod:delete',
	'mod:show-file',
	'category:save',
	'categories:fetch',
	'categories:auto-add',
	'tag:save',
	'tags:fetch',
	'mods:sync-progress',
	'web-search:mod',
];

// Expose ipcRenderer to the client
contextBridge.exposeInMainWorld('ipcRenderer', {
	send: (channel, data) => {
		if (validChannels.includes(channel)) {
			ipcRenderer.send(channel, data)
		}
	},
	on: (channel, data) => {
		if (validChannels.includes(channel)) {
			ipcRenderer.on(channel, data)
		}
	},
	off: (channel) => {
		if (validChannels.includes(channel)) {
			ipcRenderer.removeAllListeners(channel)
		}
	},
	receive: (channel, func) => {
		if (validChannels.includes(channel)) {
			// Deliberately strip event as it includes `sender`
			ipcRenderer.on(channel, (event, ...args) => func(...args))
		}
	}
});
