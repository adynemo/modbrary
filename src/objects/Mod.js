import { v4 as uuidv4 } from 'uuid';

class Mod {
	constructor(filename) {
		this.uid = uuidv4();
		this.filename = filename;
		this.name = '';
		this.dir = '';
		this.path = '';
		this.size = 0;
		this.image = undefined;
		this.category = '';
		this.tags = [];

		this.sync = false;
	}
}

export default Mod;
