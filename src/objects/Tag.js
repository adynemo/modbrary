import { v4 as uuidv4 } from 'uuid';

class Tag {
	constructor(name) {
		this.name = name;
		this.uid = uuidv4();
	}
}

export default Tag;
