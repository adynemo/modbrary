import { v4 as uuidv4 } from 'uuid';

class Category {
	constructor(name) {
		this.name = name;
		this.uid = uuidv4();
	}
}

export default Category;
