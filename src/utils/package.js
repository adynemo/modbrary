import fs from "fs";
import {Package, RawResource, StringTableResource} from "@s4tk/models";
import {getMimeTypeFromBuffer} from "./images";

// todo: import only necessary code from @s4tk/models in app
class ModData {
	constructor() {
		this.name = undefined;
		this.image = undefined;
	}
	isComplete() {
		return this.name && this.image;
	}
}

const ImageMaxSize = 10000;

const extractData = async modPath => {
	return new Promise((resolve, reject) => {
		fs.readFile(modPath, async (err, mod) => {
			if (err) reject(err);
			const resources = Package.extractResources(mod);
			const modData = new ModData();

			const stringResource = resources.find(resource => resource.value instanceof StringTableResource && resource.value.entries[0]?.value);
			if (stringResource) {
				modData.name = stringResource.value.entries[0].value;
			}

			const rawResources = resources.filter(resource => resource.value instanceof RawResource && ImageMaxSize > resource.value.sizeDecompressed);
			rawResources.sort((a, b) => b.value.sizeDecompressed - a.value.sizeDecompressed);
			while (!modData.isComplete() && rawResources.length) {
				const resource = rawResources.shift();
				const mime = await getMimeTypeFromBuffer(resource.value.buffer);
				modData.image = mime ? resource.value.buffer : undefined;
			}

			resolve(modData);
		});
	})
}

export {
	extractData
};
