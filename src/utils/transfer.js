import log from 'electron-log';
import {fetchModsDB, progressBar, saveDB} from "./mods";
import fs from "fs";
import path from "path";
import * as settings from "./settings";
import * as categories from "./categories";
import {imagesPath, modsDBPath, tmpImagesPath, transferDBPath} from "./core";
import {uniqueId} from "./easyUtils";
import {mkdir} from "./utils";
const transferLog = log.scope('transfer');

const sync = mods => {
	return new Promise((resolve, reject) => {
		const rejectError = error => {
			if (error) {
				transferLog.error(error.message);
				reject({
					success: false,
					message: 'Unable to transfer mod',
					reason: error
				});
			}
		};
		settings.fetch()
			.then(appSettings => {
				categories.fetch()
					.then(result => {
						const categories = result.data;
						const modsPath = appSettings.data.modsPath;
						const modsDB = fetchModsDB();
						let loop = 0;
						progressBar(loop, mods.length);
						for (const mod of mods) {
							transferLog.debug(`Process transfer: ${mod.filename}`);
							let subDir = '';
							const cat = categories.find(current => current.uid === mod.category);
							if (mod.category && cat) {
								subDir = `${cat.name}${path.sep}`;
							}
							const fullDir = `${modsPath}${path.sep}${subDir}`;
							let dest = `${modsPath}${path.sep}${subDir}${mod.filename}`;
							mkdir(fullDir)
								.then(() => {
									fs.access(dest, fs.constants.F_OK, (err) => {
										if (!err) {
											dest = `${modsPath}${path.sep}${uniqueId()}_${mod.filename}`;
										}
										fs.copyFile(mod.path, dest, rejectError);
									});
								}).catch(rejectError);
							if (mod.image) {
								fs.copyFile(`${tmpImagesPath}${path.sep}${mod.image}`, `${imagesPath}${path.sep}${mod.image}`, rejectError);
							}
							modsDB.push(mod);
							progressBar(++loop, mods.length);
						}
						saveDB(modsDB)
							.then(() => {
								transferLog.debug(`Packages synchronized: ${mods.length}`);
								resolve({
									success: true,
									message: `Mods are moved successfully in ${modsDBPath}`,
									data: {count: mods.length}
								});
							}).catch(rejectError);
					}).catch(rejectError)
			}).catch(rejectError);
	});
};

const reset = () => {
	const handleError = error => {
		if (error) {
			transferLog.warn(error.message);
		}
	}
	fs.rm(transferDBPath, handleError);
	fs.rm(tmpImagesPath, { recursive: true, force: true }, function(error) {
		if (error) {
			handleError(error);
		} else {
			fs.mkdir(tmpImagesPath, handleError);
		}
	});

}

export {
	sync,
	reset,
};
