import {isExist} from "./utils";
import {tagsPath} from "./core";
import fs from "fs";
import Tag from "../objects/Tag";

const fetchTags = () => {
	return new Promise((resolve, reject) => {
		if (!isExist(tagsPath)) {
			resolve([]);
			return;
		}

		fs.readFile(tagsPath, {encoding: 'utf-8'}, (err, data) => {
			if (err) reject(err);
			resolve(JSON.parse(data));
		})
	});
};

const save = async tag => {
	tag = new Tag(tag);
	const error = err => {
		return {
			success: false,
			message: `Save category failed!`,
			reason: err
		};
	}
	return new Promise((resolve, reject) => {
		fetchTags()
			.then(tags => {
				tags.push(tag);
				fs.writeFile(tagsPath, JSON.stringify(tags), (err) => {
					if (err) {
						reject(error(err));
					}
					resolve({
						success: true,
						message: `Tag saved successfully in ${tagsPath}`,
						data: tag,
					});
				});
			})
			.catch(err => {
				reject(error(err));
			})
	});
};

const fetch = async () => {
	return new Promise((resolve, reject) => {
		fetchTags()
			.then(settings => {
				resolve({
					success: true,
					message: `Tags are fetched`,
					data: settings
				});
			})
			.catch(e => {
				reject({
					success: false,
					message: `Fetch tags failed!`,
					reason: e
				});
			})
	});
};

export {
	save,
	fetch,
};
