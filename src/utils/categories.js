import {isExist} from "./utils";
import {categoriesPath} from "./core";
import * as settings from './settings';
import fs from "fs";
import Category from "../objects/Category";
import path from "path";
import {TreeNode} from "./tree";

const buildTree = rootPath => {
	const cats = [];
	const currentNode = new TreeNode(rootPath);
	const children = fs.readdirSync(currentNode.path);

	for (const child of children) {
		const childPath = `${currentNode.path}${path.sep}${child}`;
		const childStats = fs.statSync(childPath);

		if (childStats.isDirectory()) {
			cats.push(new Category(child));
		}
	}

	return cats;
}

const fetchCategories = () => {
	return new Promise((resolve, reject) => {
		if (!isExist(categoriesPath)) {
			resolve([]);
			return;
		}

		fs.readFile(categoriesPath, {encoding: 'utf-8'}, (err, data) => {
			if (err) reject(err);
			resolve(JSON.parse(data));
		})
	});
};

const save = category => {
	category = new Category(category);
	return new Promise((resolve, reject) => {
		const rejectError = (error) => {
			reject({
				success: false,
				message: `Save category failed!`,
				reason: error
			});
		}
		fetchCategories()
			.then(categories => {
				categories.push(category);
				fs.writeFile(categoriesPath, JSON.stringify(categories), (error) => {
					if (error) rejectError(error);
					resolve({
						success: true,
						message: `Category saved successfully in ${categoriesPath}`,
						data: category
					});
				});
			})
			.catch(error => rejectError(error));
	})
};

const fetch = () => {
	return new Promise((resolve, reject) => {
		fetchCategories()
			.then(categories => resolve({
				success: true,
				message: `Categories are fetched`,
				data: categories
			}))
			.catch(error => reject({
				success: false,
				message: `Fetch categories failed!`,
				reason: error
			}));
	});
};

const sync = () => {
	return new Promise((resolve, reject) => {
		const rejectError = (error) => {
			reject({
				success: false,
				message: `Sync categories failed!`,
				reason: error
			});
		}
		fetchCategories()
			.then(categories => {
				settings.fetch().then(settings => {
					const newCats = buildTree(settings.data.modsPath);
					for (const cat of newCats) {
						if (-1 === categories.findIndex(current => current.name === cat.name)) {
							categories.push(cat);
						}
					}

					fs.writeFile(categoriesPath, JSON.stringify(categories), (error) => {
						if (error) rejectError(error);
						resolve({
							success: true,
							message: `Categories saved successfully in ${categoriesPath}`,
						});
					});
				}).catch(rejectError);
			}).catch(rejectError);
	});
};

export {
	save,
	fetch,
	sync,
};
