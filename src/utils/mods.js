import {isExist} from "./utils";
import {buildTree} from "./tree";
import fs from "fs";
import Mod from "../objects/Mod";
import {fetchConvertedImages, upload, uploadFromBuffer, remove as removeImage} from "./images";
import {imagesPath, modExt, modsDBPath, transferDBPath} from "./core";
import * as settings from './settings';
import * as categories from './categories';
import {extractData} from "./package";
import path from "path";
import {BrowserWindow} from "electron";
import log from 'electron-log';
const modLog = log.scope('mod');

const getTree = (searchPath) => {
	if (!isExist(searchPath)) {
		modLog.warn(`Mods path not exists (${searchPath})`);
		return false;
	}

	const tree = buildTree(searchPath, modExt);
	let parent;
	const stack = [tree];
	const list = [];
	while (stack.length) {
		const current = stack.pop();

		if (current.isDirectory && !current.children.length) {
			continue;
		}

		if (current.isDirectory && current.children.length) {
			parent = current;
			for (const child of current.children) {
				if (!child.isDirectory) {
					const mod = new Mod(child.name);
					mod.dir = parent.name;
					mod.path = child.path;
					mod.size = child.size;
					list.push(mod);
				} else {
					stack.push(child);
				}
			}
		} else {
			const mod = new Mod(current.name);
			mod.dir = parent ? parent.name : '';
			mod.path = current.path;
			mod.size = current.size;
			list.push(mod);
		}
	}

	return list;
}

const fetchModsDB = (isDefault = true) => {
	const searchPath = isDefault ? modsDBPath : transferDBPath;
	if (!isExist(searchPath)) {
		return [];
	}

	return JSON.parse(fs.readFileSync(searchPath, {encoding: 'utf-8'}));
};

const getAll = (isDefault = true) => {
	try {
		const mods = fetchModsDB(isDefault);
		return {
			success: true,
			message: 'Mods fetched from database',
			data: {mods, isDefault},
		};
	} catch (e) {
		return {
			success: false,
			message: 'Fetch mods from database failed',
			reason: e,
		};
	}
};

const edit = async (mod, image, isDefault = true) => {
	try {
		if (image) {
			const result = await upload(image, isDefault);
			if (!result.success) {
				return result;
			}
			if (mod.image) {
				await removeImage(mod.image, isDefault);
			}
			mod.image = result.data.filename;
		}

		if (typeof mod.base64 !== 'undefined') {
			delete mod.base64;
		}

		const modsDB = fetchModsDB(isDefault);
		mod.sync = true;
		const i = modsDB.findIndex(modDB => modDB.uid === mod.uid);
		if (-1 === i) {
			modsDB.push(mod);
		} else {
			modsDB[i] = mod;
		}

		const DBPath = isDefault ? modsDBPath : transferDBPath;
		fs.writeFileSync(DBPath, JSON.stringify(modsDB));

		const result = await fetchConvertedImages([mod], isDefault);
		const editedMod = result.success ? result.data.mods[0] : mod;
		return {
			success: true,
			message: `Mod is saved successfully in ${DBPath}`,
			data: {mod: editedMod}
		};
	} catch (e) {
		return {
			success: false,
			message: `Mod edit failed!`,
			reason: e
		};
	}
};

const progressBar = (count, total) => {
	try {
		BrowserWindow.getFocusedWindow().webContents.send('mods:sync-progress', {count, total})
	} catch (e) {
		modLog.debug('Progress bar failed');
	}
}

const saveDB = (mods, isDefault = true) => {
	return new Promise((resolve, reject) => {
		fs.writeFile(isDefault ? modsDBPath : transferDBPath, JSON.stringify(mods), (err) => {
			if (err) {
				reject(err);
				return;
			}
			modLog.debug(`Packages saved in database: ${mods.length}`);
			resolve(true);
		});
	})
};

const sync = async (syncPath = null) => {
	return new Promise((resolve, reject) => {
		settings.fetch()
			.then(async appSettings => {
				const isDefault = null === syncPath;
				const modsPath = syncPath || appSettings.data.modsPath;
				const tree = await getTree(modsPath);
				const modsDB = fetchModsDB(isDefault);
				const newModsDB = [...modsDB];
				let cats = [];
				if (isDefault) {
					const result = await categories.fetch();
					cats = result?.data || [];
				}

				for (const mod of modsDB) {
					const i = tree.findIndex(modDB => modDB.path === mod.path);
					if (-1 === i) {
						modLog.debug(`${mod.filename} not found on disk. Remove from database.`);
						newModsDB.splice(newModsDB.indexOf(mod), 1);
						if (mod.image) {
							removeImage(mod.image, isDefault)
						}
					}
				}

				let loop = 0;
				let newModsLoop = 0;
				const treeCount = tree.length;
				progressBar(loop, treeCount);
				for (const mod of tree) {
					modLog.debug(`Process package: ${mod.filename}`);
					const i = modsDB.findIndex(modDB => modDB.path === mod.path);
					if (-1 === i) {
						mod.sync = true;
						if (!mod.image) {
							const packageData = await extractData(mod.path);
							if (packageData.image) {
								mod.image = await uploadFromBuffer(packageData.image, isDefault);
							}
							mod.name = mod.name || packageData.name;
							if (!mod.name) {
								mod.name = mod.filename.substring(0, mod.filename.indexOf('.package'))
							}
							if (mod.dir && isDefault) {
								const cat = cats.find(current => current.name === mod.dir);
								mod.category = cat.uid;
							}
						}
						newModsDB.push(mod);
						++newModsLoop;
						if (newModsLoop % 10 === 0) {
							saveDB(newModsDB, isDefault);
						}
					}
					progressBar(++loop, treeCount);
				}

				saveDB(newModsDB, isDefault)
					.then(() => {
						modLog.debug(`Packages synchronized: ${newModsDB.length}`);
						resolve({
							success: true,
							message: `Mod is saved successfully in ${isDefault ? modsDBPath : transferDBPath}`
						});
					})
					.catch(err => {
						throw err;
					});
			})
			.catch(e => {
				reject({
					success: false,
					message: `Mod save failed!`,
					reason: e
				});
			});
	});
};

const remove = mod => {
	return new Promise((resolve, reject) => {
		const rejectError = error => {
			reject({
				success: false,
				message: 'Unable to remove mod',
				reason: error
			});
		};
		const modsDB = fetchModsDB();
		const i = modsDB.findIndex(value => value.uid === mod.uid);
		if (-1 === i) {
			rejectError('Mod not found in DB');
		}

		modsDB.splice(i, 1);
		fs.writeFile(modsDBPath, JSON.stringify(modsDB), (error) => {
			if (error) rejectError(error);
			modLog.debug(`Mod removed from database: ${mod.name}`);
			fs.unlink(`${imagesPath}${path.sep}${mod.image}`, (error) => {
				if (error) modLog.warn(error);
				fs.unlink(mod.path, (error) => {
					if (error) rejectError(error);
					modLog.debug(`Package deleted: ${mod.path}`);
					resolve({
						success: true,
						message: 'Mod removed'
					});
				});
			});
		});
	});
}

export {
	getTree,
	getAll,
	edit,
	sync,
	remove,
	fetchModsDB,
	saveDB,
	progressBar
};
