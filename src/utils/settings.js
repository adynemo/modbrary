import fs from 'fs';
import { isExist } from "./utils";
import { settingsPath } from "./core";
import log from 'electron-log';

const fetchSettings = () => {
	return new Promise((resolve, reject) => {
		if (!isExist(settingsPath)) {
			resolve({});
			return;
		}

		fs.readFile(settingsPath, {encoding: 'utf-8'}, (err, data) => {
			if (err) reject(err);
			resolve(JSON.parse(data));
		})
	});
};

const save = async data => {
	try {
		let settings = await fetchSettings();
		settings = Object.assign(settings, data);
		fs.writeFileSync(settingsPath, JSON.stringify(settings));
		return {
			success: true,
			message: `Settings are saved successfully in ${settingsPath}`
		};
	} catch (err) {
		console.error(err);
		return {
			success: false,
			message: `Settings save failed!`,
			reason: err
		};
	}
};

const fetch = () => {
	return new Promise((resolve, reject) => {
		fetchSettings()
			.then(settings => {
				resolve({
					success: true,
					message: 'Settings are fetched',
					data: settings
				});
			})
			.catch(e => {
				const error = {
					success: false,
					message: 'Settings fetch failed!',
					reason: e
				};
				log.error(error);
				reject(error);
			})
	});
};

const SearchEngineMap = {
	ddg: 'https://duckduckgo.com/?q=',
	google: 'https://www.google.com/search?q=',
};
const getSearchEngineURI = () => {
	return new Promise((resolve, reject) => {
		fetchSettings()
			.then(settings => {
				resolve({
					success: true,
					message: 'Search engine are fetched',
					data: {uri: SearchEngineMap[settings.search || 'ddg']}
				});
			})
			.catch(e => {
				const error = {
					success: false,
					message: 'Search engine fetch failed!',
					reason: e
				};
				log.error(error);
				reject(error);
			})
	});
}

export {
	save,
	fetch,
	getSearchEngineURI,
};
