import {app} from "electron";
import path from "path";
import { mkdir } from "./utils";

const storagePath = `${app.getPath('userData')}${path.sep}storage`;

const getFullStoragePath = filename => `${storagePath}${path.sep}${filename}`;

const settingsFile = 'settings.json';
const settingsPath = getFullStoragePath(settingsFile);

const modsDB = 'mods.json';
const modsDBPath = getFullStoragePath(modsDB);
const transferDB = 'transfer.json';
const transferDBPath = getFullStoragePath(transferDB);
const modExt = '.package';

const imagesPath = getFullStoragePath('images');
const tmpImagesPath = getFullStoragePath('images-tmp');

const categoriesFile = 'categories.json';
const categoriesPath = getFullStoragePath(categoriesFile);

const tagsFile = 'tags.json';
const tagsPath = getFullStoragePath(tagsFile);

const now = new Date();
const logFilename = `${now.toLocaleDateString()}.log`;
const logFullPath = `${app.getPath('userData')}${path.sep}logs${path.sep}${logFilename}`;

const setup = async () => {
	if (!await mkdir(storagePath)) {
		return {
			success: false,
			message: 'Failed to create storage directory!'
		};
	}
	if (!await mkdir(imagesPath)) {
		return {
			success: false,
			message: 'Failed to create image directory!'
		};
	}
	if (!await mkdir(tmpImagesPath)) {
		return {
			success: false,
			message: 'Failed to create temporary image directory!'
		};
	}

	return {
		success: true,
		message: 'Setup complete!'
	};
}

export {
	storagePath,
	settingsPath,
	modsDBPath,
	modExt,
	imagesPath,
	categoriesPath,
	tagsPath,
	logFullPath,
	transferDBPath,
	tmpImagesPath,
	setup,
};
