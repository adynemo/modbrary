import fs from "fs";
import path from "path";

class TreeNode {
	constructor(path) {
		this.path = path;
		this.name = '';
		this.isDirectory = true;
		this.ext = null;
		this.size = 0;
		this.children = [];
	}
}

const buildTree = (rootPath, ext) => {
	const root = new TreeNode(rootPath);
	const stack = [root];

	while (stack.length) {
		const currentNode = stack.pop();

		if (currentNode) {
			const children = fs.readdirSync(currentNode.path);

			for (const child of children) {
				const childPath = `${currentNode.path}${path.sep}${child}`;
				const childStats = fs.statSync(childPath);

				if (
					ext &&
					!childStats.isDirectory() &&
					path.extname(childPath) !== ext
				) {
					continue;
				}

				const childNode = new TreeNode(childPath);
				childNode.name = child;
				currentNode.children.push(childNode);

				if (fs.statSync(childNode.path).isDirectory()) {
					stack.push(childNode);
				} else {
					childNode.isDirectory = false;
					childNode.ext = ext;
					childNode.size = childStats.size;
				}
			}
		}
	}

	return root;
}

export {
	TreeNode,
	buildTree,
};
