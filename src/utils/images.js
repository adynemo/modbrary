import fs from "fs";
import mime from 'mime-types';
import { v4 as uuidv4 } from 'uuid';
import path from "path";
import {imagesPath, tmpImagesPath} from "./core";
import {fromBuffer} from "detect-file-type";
import log from 'electron-log';

const getMimeTypeFromBuffer = buffer => {
	return new Promise((resolve, reject) => {
		fromBuffer(buffer, (err, result) => {
			if (err) {
				reject(err);
			}

			resolve(result && /^image\/.+/.test(result.mime) ? result.mime : undefined);
		})
	})
};

const convert = async filepath => {
	try {
		const mimeType = mime.lookup(filepath);
		const base64 = fs.readFileSync(filepath).toString('base64');
		return {
			success: true,
			message: 'Image correctly encoded',
			data: `data:${mimeType};base64,${base64}`
		};
	} catch (e) {
		const error = {
			success: false,
			message: `Image copy failed!`,
			reason: e
		};
		log.warn(error);
		return error;
	}
};

const upload = async (filepath, isDefault = true) => {
	try {
		const name = uuidv4();
		const ext = path.extname(filepath);
		const filename = `${name}${ext}`;
		const basePath = isDefault ? imagesPath : tmpImagesPath;
		const newImagePath = `${basePath}${path.sep}${filename}`;
		fs.copyFileSync(filepath, newImagePath);
		return {
			success: true,
			message: `Image are copied successfully in ${basePath}`,
			data: {filename}
		};
	} catch (e) {
		const error = {
			success: false,
			message: `Image upload failed!`,
			reason: e
		};
		log.warn(error);
		return error;
	}
};

const mimeTypesMapping = {
	'image/jpeg': '.jpeg',
	'image/png': '.png',
}

const uploadFromBuffer = async (buffer, isDefault = true) => {
	const basePath = isDefault ? imagesPath : tmpImagesPath;
	const mimeType = await getMimeTypeFromBuffer(buffer);
	if (!mimeType) log.warn('Mime type unrecognized');
	const ext = mimeTypesMapping[mimeType];
	if (!ext) log.warn('Extension unrecognized');
	const filename = `${uuidv4()}${ext}`;
	const fullpath = `${basePath}${path.sep}${filename}`;
	fs.writeFileSync(fullpath, buffer, 'utf8');

	return filename;
}

const fetchConvertedImages = async (mods, isDefault = true) => {
	try {
		const convertedMods = [];
		for (const mod of mods) {
			if (mod.image) {
				const filepath = `${isDefault ? imagesPath : tmpImagesPath}${path.sep}${mod.image}`;
				const data = await convert(filepath);
				mod.base64 = data.data;
			}
			convertedMods.push(mod);
		}
		return {
			success: true,
			message: `Images fetched and converted successfully!`,
			data: {mods: convertedMods}
		};
	} catch (e) {
		return {
			success: false,
			message: `Get image failed!`,
			reason: e
		};
	}
};

const remove = (filename, isDefault) => {
	return new Promise((resolve, reject) => {
		const basePath = isDefault ? imagesPath : tmpImagesPath;
		const fullPath = `${basePath}${path.sep}${filename}`;
		fs.access(fullPath, fs.constants.F_OK, (err) => {
			if (!err) {
				fs.rm(fullPath, (error) => {
					if (error) {
						log.warn(`Unable to remove image: ${error.message}`);
						reject(false);
					} else {
						resolve(true);
					}
				});
			} else {
				log.warn(`Unable to remove image: ${err.message}`);
				reject(false);
			}
		});
	})
}

export {
	convert,
	upload,
	uploadFromBuffer,
	fetchConvertedImages,
	getMimeTypeFromBuffer,
	remove,
};
