import fs from 'fs'

// Create a directory, returning the result of creating a directory
const mkdir = path => {
	return new Promise((resolve, reject) => {
		if (fs.existsSync(path)) {
			resolve(true)
			return
		}
		fs.mkdir(path, (error) => {
			if (error) {
				reject(false)
			} else {
				resolve(true)
			}
		})
	})
};

const isExist = path => fs.existsSync(path);

export {
	mkdir,
	isExist,
}
