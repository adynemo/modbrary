import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import {
  mdiHomeOutline,
  mdiBookOutline,
  mdiShapeOutline,
  mdiTagOutline,
  mdiCogs,
  mdiInformationOutline,
  mdiTransfer
} from "@mdi/js";

const routes = [
  {
    path: '/',
    name: 'Home',
    component: HomeView,
    meta: {
      icon: mdiHomeOutline
    }
  },
  {
    path: '/library',
    name: 'Library',
    component: () => import('../views/LibraryView.vue'),
    meta: {
      icon: mdiBookOutline
    }
  },
  {
    path: '/categories',
    name: 'Categories',
    component: () => import('../views/CategoriesView.vue'),
    meta: {
      icon: mdiShapeOutline
    }
  },
  {
    path: '/tags',
    name: 'Tags',
    component: () => import('../views/TagsView.vue'),
    meta: {
      icon: mdiTagOutline
    }
  },
  {
    path: '/transfer',
    name: 'Transfer',
    component: () => import('../views/TransferView.vue'),
    meta: {
      icon: mdiTransfer
    }
  },
  {
    path: '/settings',
    name: 'Settings',
    component: () => import('../views/SettingsView.vue'),
    meta: {
      icon: mdiCogs
    }
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/AboutView.vue'),
    meta: {
      icon: mdiInformationOutline
    }
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
