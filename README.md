# Modbrary

Modbrary is a library to manage your Sims 4's mods.

## Features

- List all mods with original names and images
- Sort mods by categories and tags
- Delete mods
- Handle light and dark theme

## Download

Download the Windows exe in [build](./build) directory.

## Support

Please, submit your issue in [GitLab](https://gitlab.com/adynemo/modbrary/-/issues/new?issue).

## Development

### Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn electron:serve
```

### Compiles and minifies for production
```
yarn electron:build -w
```

### Lints and fixes files
```
yarn lint
```
