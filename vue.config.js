// const { VuetifyLoaderPlugin } = require('vuetify-loader');
module.exports = {
	productionSourceMap: process.env.NODE_ENV !== 'production',
	pluginOptions: {
    // vuetify: [new VuetifyLoaderPlugin({ autoImport: true })],
		electronBuilder: {
			preload: 'src/preload.js',
			chainWebpackMainProcess: config => {
				config.module
					.rule('babel')
					.test(/\.js$/)
					.use('babel')
					.loader('babel-loader')
					.options({
						presets: [['@babel/preset-env']],
					})
			},
			mainProcessWatch: ['src/background.js', 'src/utils/*.js'],
			builderOptions: {
				"appId": "com.modbrary.app",
				"productName": "Modbrary",//Project name, which is also the name of the generated installation file, aDemo.exe
				"copyright": "Copyright © 2022",//Copyright Information
				"directories": {
					"output": "build"//Output File Path
				},
				"win": {//win-related configuration
					"target": [
						{
							"target": "nsis",//Making Installer with nsis
							"arch": [
								"x64",//64-bit
							]
						}
					]
				},
				"linux": {//linux-related configuration
					"category": "Office",
					"target": [
						{
							"target": "deb",//Making Installer with nsis
							"arch": [
								"x64",//64-bit
							]
						},
						{
							"target": "AppImage",//Making Installer with nsis
							"arch": [
								"x64",//64-bit
							]
						}
					]
				},
				"nsis": {
					"oneClick": false, // Is one-click installation possible
					"allowElevation": true, // Allow requests for elevation.If false, the user must restart the installer with elevated privileges.
					"allowToChangeInstallationDirectory": true, // Allow modification of installation directory
					"installerIcon": "./build/icons/icon.ico",// Installation Icon
					"uninstallerIcon": "./build/icons/icon.ico",//Uninstall Icon
					"installerHeaderIcon": "./build/icons/icon.ico", // Header icon at installation
					"createDesktopShortcut": true, // Create desktop icons
					"createStartMenuShortcut": true,// Create Start Menu Icon
					"shortcutName": "Modbrary", // Desktop Display Application Name
				},
			}
		},
  }
}
